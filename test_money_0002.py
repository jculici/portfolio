def test_multiplication():
    # test that you can multiply a Dollar by a number and get the right amount.
    five = Dollar(5)
    ten = five.times(2)
    assert 10 == ten.amount 
    fifteen = five.times(3)
    assert 15 == fifteen.amount