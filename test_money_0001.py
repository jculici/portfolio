def test_multiplication():
    # test that you can multiply a Dollar by a number and get the right amount.
    five = Dollar(5)
    five.times(2)
    assert 10 == five.amount 
    five.times(3)
    assert 15 == five.amount