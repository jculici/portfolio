def add(a, b):
    """Compute the sum of two numbers.

    Usage examples:
    >>> add(4.0, 2.0)
    6.0
    >>> add(1, 3)
    5
    """
    return a + b